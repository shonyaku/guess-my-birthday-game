from random import randint

name = input("Hi! What is your name? ")

# Guess 1

month_guess = randint(1, 12)
year_guess = randint(1924, 2004)

print("Guess 1: ", name, "were you born in", month_guess, "/", year_guess, "?")

player_answer = input("yes or no? ")

if player_answer == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Guess 2

year_guess = randint(1924, 2004)
month_guess = randint(1, 12)

print("Guess 2: ", name, "were you born in", month_guess, "/", year_guess, "?")

player_answer = input("yes or no? ")

if player_answer == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Guess 3

year_guess = randint(1924, 2004)
month_guess = randint(1, 12)

print("Guess 3: ", name, "were you born in", month_guess, "/", year_guess, "?")
player_answer = input("yes or no? ")

if player_answer == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Guess 4

year_guess = randint(1924, 2004)
month_guess = randint(1, 12)

print("Guess 4: ", name, "were you born in", month_guess, "/", year_guess, "?")
player_answer = input("yes or no? ")

if player_answer == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Lemme try again!")

# Guess 5

year_guess = randint(1924, 2004)
month_guess = randint(1, 12)

print("Guess 5: ", name, "were you born in", month_guess, "/", year_guess, "?")
player_answer = input("yes or no? ")

if player_answer == "yes":
    print("I knew it!")
    exit()
else:
    print("I have other things to do. Good bye.")
    exit()