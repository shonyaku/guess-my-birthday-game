from random import randint
guess_counter = 1
name = input("Hi! What is your name? ")

# start of greater gameplay loop

for guess_counter in range(1, 5):

    year_guess = randint(1924, 2004)
    month_guess = randint(1, 12)
    str_month_guess = str(month_guess)
    str_year_guess = str(year_guess)
    str_guess_counter = str(guess_counter)

    print("Guess " + str_guess_counter + " : " + name + " were you born in " + str_month_guess + " / " + str_year_guess + " ?")
    player_answer = input("yes or no? ")

    if player_answer == "yes":
        print("I knew it!")
        exit()
    else:
        print("Drat! Lemme try again!")
        guess_counter = guess_counter + 1

print("I have other things to do. Good bye.")
exit()